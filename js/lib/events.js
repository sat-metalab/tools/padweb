define([
  'jquery',
  'handlers',
  'config',
  // 'jhammer'
  'hammerjs',
  'lodash'
], function($, handlers, config, Hammer, _) {

  var updateRate = config.updateRate;
  var currentPage = 0;
  var pagesCount = 2;
  var slideOn = true;
  var widthW = 0;
  var heightW = 0;
  var calibrationState = false;
  var rotationValue = null;
  var heightValue = null;
  // elements to which we will attach events
  var cEl = $("#calibration")[0];
  var nEl = $("#navigation")[0];
  // events on calibration screen
  var ch = new Hammer.Manager(cEl);
  // events on navigation screen
  var nh = new Hammer.Manager(nEl);
  
  // mc.add(new Hammer.Tap({event: 'doubletap', taps: 2, pointers: 1}));
  // mc.on("doubletap", function(ev) {
  //   console.log("tap", ev.type);
  // });

  var init = function (w, h) {
    widthW = w;
    heightW = h;
    getGyro();
    getCalibrationSlider();
    initEvents();
  };

  var setCalibrationEvents = function () {
    ch.add(new Hammer.Swipe({
      event: 'swipeleft', 
      pointers: 2, 
      direction: Hammer.DIRECTION_LEFT,
      threshold: config.swipeDistanceThreshold,
      velocity: config.swipeVelocityThreshold
    }));
    ch.on('swipeleft', handlers.handleSwipeLeft);
  };

  var setNavigationEvents = function() {
    nh.add(new Hammer.Swipe({
      event: 'swiperight', 
      direction: Hammer.DIRECTION_RIGHT,
      pointers: 2,         
      threshold: config.swipeDistanceThreshold,
      velocity: config.swipeVelocityThreshold
    }));

    nh.add(new Hammer.Pan({
      pointers: 1,
      direction: Hammer.DIRECTION_VERTICAL
    }));
    

    nh.add(new Hammer.Tap({
      event: 'doubletap',
      taps: 2,
      pointers: 1
    }));
    
    nh.on('swiperight', handlers.handleSwipeRight);
    nh.on('doubletap', handlers.handleDoubleTap);
    nh.on('panmove', handlers.handleMove);
    nh.on('panend', handlers.handleStop);
  };

  var initEvents = function() {
    setCalibrationEvents();
    setNavigationEvents();
  };

  var getGyro = function() {
    window.addEventListener("deviceorientation", _.throttle(handlers.handleOrientation, updateRate));
  };

  // var getButton = function (button) {
    
  // };

  var getCalibrationSlider = function () {
    $("#calibrateRotation-slider").on("change", $.throttle(function() {
      var id = this.id;
      rotationValue = $(this).val();
      handlers.handleCalibrationActions(calibrationState, id, rotationValue);
    }, updateRate));
    $("#calibrateHeight-slider").on("change", $.throttle(function() {
      var id = this.id;
      heightValue = $(this).val();
      handlers.handleCalibrationActions(calibrationState, id, heightValue);
    }, updateRate));
    // if($("#calibrationBtn .icon").hasClass("pressed")) {
    //   calibrationState = true;
    // } else {
    //   calibrationState = false;
    // };
    console.log("events: ", rotationValue, heightValue);

  };

  var getCalibrationButton = function () {
    console.log("calibration button touched");
    $("#calibrationBtn .icon").toggleClass("pressed");
    $("#meshRotation-slider").toggleClass("hidden");
    $("#meshHeight-slider").toggleClass("hidden");
    calibrationState = !calibrationState;
    handlers.handleCalibrationActions(calibrationState, rotationValue, heightValue);
    getCalibrationSlider();
  };

  return {
    init: init,
    getCalibrationButton: getCalibrationButton
  }

});
